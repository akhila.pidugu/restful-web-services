package com.example.demo.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "Channel")
public class Channel{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String channelName;

    @Column(name = "rating")
    private int rating;

    public Channel(){

    }

    public Channel(String channelName, int rating) {
        this.channelName = channelName;
        this.rating = rating;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Channel channel = (Channel) o;
        return rating == channel.rating && Objects.equals(channelName, channel.channelName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(channelName, rating);
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}