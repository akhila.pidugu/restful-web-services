package com.example.demo.controller;

import com.example.demo.ChannelRepository;
import com.example.demo.model.Channel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ChannelController {
    @Autowired
    private ChannelRepository channelRepository;

    @GetMapping(path = "/channels")
    public  List<Channel> getChannels() {
        return channelRepository.findAll();
    }

    @PostMapping(path = "/channels")
    public Channel createChannel(@Valid @RequestBody Channel channel) {
        return channelRepository.save(channel);
    }
}
