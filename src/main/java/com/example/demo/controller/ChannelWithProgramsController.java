package com.example.demo.controller;

import com.example.demo.model.ChannelWithPrograms;
import com.example.demo.DAO.ChannelWithProgramsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController

public class ChannelWithProgramsController {
    @Autowired
    private ChannelWithProgramsDAO channelWithProgramsDAO;

    @RequestMapping(path = "/channelwithprograms")
    public Map<String, List<ChannelWithPrograms>> getChannelWithPrograms() {
        return channelWithProgramsDAO.getAllChannelWithPrograms();
    }

    public Map<String, List<ChannelWithPrograms>> getAddedChannelWithPrograms() {
        return channelWithProgramsDAO.addChannel();
    }
}
