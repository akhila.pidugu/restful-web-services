package com.example.demo.controller;

import com.example.demo.model.TVProgram;
import com.example.demo.DAO.TVProgramDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController

public class TVProgramController {
    @Autowired
    private TVProgramDAO tvProgramDAO;

    @RequestMapping(path = "/tvprograms")
    public Map<String, List<TVProgram>> getTVPrograms() {
        return tvProgramDAO.getAllPrograms();
    }

}

