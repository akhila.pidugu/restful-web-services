package com.example.demo;

import com.example.demo.model.Channel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface ChannelRepository extends JpaRepository<Channel, Long> {
}