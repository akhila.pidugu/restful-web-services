package com.example.demo.DAO;

import com.example.demo.model.ChannelWithPrograms;
import com.example.demo.model.TVProgram;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChannelWithProgramsDAO {
    public static List<ChannelWithPrograms> channelWithProgramsList = new ArrayList<>(Arrays.asList(
            new ChannelWithPrograms("Work",
                    10,
                    Arrays.asList(new TVProgram(
                                    "The Key Tee Party",
                                    4
                            ),

                            new TVProgram(
                                    "The Lockdown",
                                    1
                            ))
            ),
            new ChannelWithPrograms(
                    "Life",
                    10,
                    Arrays.asList(
                            new TVProgram(
                                    "The Project",
                                    5
                            ),

                            new TVProgram(
                                    "The Intern",
                                    5
                            ),

                            new TVProgram(
                                    "The Training",
                                    1
                            )
                    ))));

    public Map<String, List<ChannelWithPrograms>> getAllChannelWithPrograms() {
        HashMap<String, List<ChannelWithPrograms>> map = new HashMap<>();
        map.put("items", channelWithProgramsList);
        return map;
    }

    public Map<String, List<ChannelWithPrograms>> addChannel() {
        List<ChannelWithPrograms> channelWithPrograms = new ArrayList<>(channelWithProgramsList);
        channelWithPrograms.addAll(Arrays.asList(
                new ChannelWithPrograms("AfterLife",
                        10,
                        Arrays.asList()
                )));
        HashMap<String, List<ChannelWithPrograms>> map = new HashMap<>();
        map.put("items", channelWithPrograms);
        return map;
    }
}
